public class student {
	private double grade;
	private String name;
	private String course;
	public int amountLearnt;
	
	public student(String name, double grade) //constructor
	{
		this.grade = grade;
		this.name = name;
		this.course = "Not selected";
		this.amountLearnt = 0;
	}
	public void setName(String newName) {
		this.name = newName;
	}
	
	public void setGrade(double newGrade) {
		this.grade = newGrade;
	}
	
	public void setCourse(String newCourse) {
		this.course = newCourse;
	}
	public void setAmountLearnt(int newAmountLearnt){
		this.amountLearnt = newAmountLearnt;
	}
	
	public String getName(){
		return this.name;
	}
	public double getGrade(){
		return this.grade;
	}
	public String getCourse(){
		return this.course;
	}
	public int getLearn(){
		return this.amountLearnt;
	}
	
	public void sayHi(double price) {
		System.out.println("hey! I'm " + name);
	}

	public void study(){
		System.out.println("I'm studying!");
		
	}
	
	public void learn(int amountStudied){
		boolean check = valid(amountStudied);
		
		if(check){
			this.amountLearnt+=amountStudied;
		}
		else{
			System.out.println("Amount studied is less than 0");
		}
		
	}
	private boolean valid(int amountStudied){
		boolean check = false;
		if(amountStudied<0){
			check = false;
		}
		else{
			check = true;
		}
		return check;
	}
	
	
}